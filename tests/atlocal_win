#
# atlocal_win gnucobol/tests
#
# This script is a special variant of the atlocal script for testing
# a not POSIX build (VisualStudio build for example) in a POSIX environment.
#
# Copyright (C) 2003-2012, 2014-2016 Free Software Foundation, Inc.
# Written by Keisuke Nishida, Roger While, Simon Sobisch
#
# This file is part of GnuCOBOL.
#
# The GnuCOBOL compiler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# GnuCOBOL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GnuCOBOL.  If not, see <http://www.gnu.org/licenses/>.

# CC="gcc -std=gnu99"

COB_SRC_PATH="$(cd ${abs_srcdir}/.. ; pwd)"
COB_WIN_BUILDPATH="$COB_SRC_PATH/build_windows/Win32/Release"

COB_BIGENDIAN="no"
COB_HAS_UTC_OFFSET="yes"

export COBC="cobc.exe"
export COBCRUN="cobcrun.exe"

TEMPLATE="${abs_srcdir}/testsuite.src"

FLAGS="-debug -Wall ${COBOL_FLAGS}"
COMPILE="${COBC} -x ${FLAGS}"
COMPILE_ONLY="${COBC} -fsyntax-only ${FLAGS}"
COMPILE_MODULE="${COBC} -m ${FLAGS}"

COB_OBJECT_EXT="obj"
COB_EXE_EXT="@COB_EXE_EXT@"

# Fix for testcases were cobc uses win32 paths internally
PATHSEP=";"

PATH="$COB_WIN_BUILDPATH:${PATH}"
export PATH

# Helper script to unify listings (repleace version, date, time)
UNIFY_LISTING="$COB_SRC_PATH/tests/listings-sed.sh"

# unset all environment variables that are used in libcob for runtime configuration
for cobenv in $($COBCRUN --runtime-conf | grep COB_ | cut -d: -f2); do unset $cobenv; done

# For running the testsuite in Cygwin with non-Cygwin binaries (for example Visual C builds)
# (not needed for running the testsuite with MSYS as this translates the path)
_return_path () {
	# For later reference only
	#echo "$1" | sed 's/\//\\/g' | sed 's/^\\\([a-z]\)\\/\1:\\/g')
	if test "$OSTYPE" = "cygwin"; then
	   cygpath -pw "$1"
	else
	   # check for WSL / Bash on Windows; ignore error messages (file not available)
	   if $(grep -q "Microsoft" /proc/sys/kernel/osrelease 2>/dev/null); then
	      echo "$1" | sed -E 's+^/mnt/(.{1})+\1:+' | sed 's+:$+:/+1'
	   else
	      echo "$1"
	   fi
	fi
}

export COB_CONFIG_DIR="$(_return_path "$COB_SRC_PATH/config")"
export COB_RUNTIME_CONFIG="$(_return_path "$COB_SRC_PATH/config/runtime_empty.cfg")"
export COB_COPY_DIR="$(_return_path "$COB_SRC_PATH/copy")"
export COB_LIBRARY_PATH="$(_return_path "$COB_WIN_BUILDPATH"):$(_return_path ":$COB_LIBRARY_PATH")"
export COB_UNIX_LF=YES
export COB_MSG_FORMAT=GCC
export COB_HAS_UTC_OFFSET
export LC_ALL=C



if test $($COBC --info | grep -i -c "ISAM.*disabled") = 0; then
   COB_HAS_ISAM="yes"
else
   COB_HAS_ISAM="no"
fi
export COB_HAS_ISAM

COB_HAS_CURSES="no"
if test "$TERM" != "cygwin"; then # this is true for CYGWIN and MSYS
   # using non-cygwin builds and MSYS builds results in
   # "Redirection is not supported"
   # workaround until solved (likely via curses library):
   # only activate when this isn't true
   if test $($COBC --info | grep -i -c "screen .*: .*curses") = 1; then
      COB_HAS_CURSES="yes"
   fi
fi
export COB_HAS_CURSES
export PATHSEP


# For rare cases where cobc/libcob may need to know if they're running in test mode:
case $0 in
	*/testsuite) export COB_IS_RUNNING_IN_TESTMODE=1;;
	*)	unset COB_IS_RUNNING_IN_TESTMODE;;
esac
