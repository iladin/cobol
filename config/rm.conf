# GnuCOBOL compiler configuration
#
# Copyright (C) 2001-2012, 2014-2017 Free Software Foundation, Inc.
# Written by Keisuke Nishida, Roger While, Simon Sobisch, Edward Hart
#
# This file is part of GnuCOBOL.
#
# The GnuCOBOL compiler is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# GnuCOBOL is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GnuCOBOL.  If not, see <http://www.gnu.org/licenses/>.


# Value: any string
name: "RM-COBOL"

# Value: enum
standard-define			6
#        CB_STD_OC = 0,
#        CB_STD_MF,
#        CB_STD_IBM,
#        CB_STD_MVS,
#        CB_STD_BS2000,
#        CB_STD_ACU,
#        CB_STD_RM
#        CB_STD_85,
#        CB_STD_2002,
#        CB_STD_2014,

# TO-DO: Allow configuring WHEN-COMPILED date format (see p. 22).

# Value: int
tab-width:			8 # check
text-column:			72 # TO-DO: add >>IMP MARGIN-R (see p. 50)
# Maximum word-length for COBOL words / Programmer defined words
# current max (COB_MAX_WORDLEN): 61
#word-length:			240
word-length:			61
# external-word-length:		30 # TO-DO: Add!
literal-length:			65535
numeric-literal-length:		30
pic-length:			30

# Value: 'mf', 'ibm'
# TO-DO: Add explanation
assign-clause:			mf

# If yes, file names are resolved at run time using
# environment variables.
# For example, given ASSIGN TO "DATAFILE", the file name will be
#  1. the value of environment variable 'DD_DATAFILE' or
#  2. the value of environment variable 'dd_DATAFILE' or
#  3. the value of environment variable 'DATAFILE' or
#  4. the literal "DATAFILE"
# If no, the value of the assign clause is the file name.
#
filename-mapping:		no

# Alternate formatting of numeric fields
pretty-display:			no

# Allow complex OCCURS DEPENDING ON
complex-odo:			yes

# Allow REDEFINES to other than last equal level number
indirect-redefines:		no

# Binary byte size - defines the allocated bytes according to PIC
# Value:         signed  unsigned  bytes
#                ------  --------  -----
# '2-4-8'        1 -  4    same        2
#                5 -  9    same        4
#               10 - 18    same        8
#
# '1-2-4-8'      1 -  2    same        1
#                3 -  4    same        2
#                5 -  9    same        4
#               10 - 18    same        8
#
# '1--8'         1 -  2    1 -  2      1
#                3 -  4    3 -  4      2
#                5 -  6    5 -  7      3
#                7 -  9    8 -  9      4
#               10 - 11   10 - 12      5
#               12 - 14   13 - 14      6
#               15 - 16   15 - 16      7
#               17 - 18   17 - 18      8
# TO-DO: What happens for 19 to 30 digits? RM-COBOL will allocate 16 bytes.
binary-size:			2-4-8

# Numeric truncation according to ANSI
binary-truncate:		yes

# Binary byte order
# Value: 'native', 'big-endian'
binary-byteorder:		big-endian

# Allow larger REDEFINES items
larger-redefines-ok:		yes # TO-DO: But only for 01 items (see p. 134)

# Allow certain syntax variations (eg. REDEFINES position)
relax-syntax-checks:		yes # TO-DO: For REDEFINES position, at least.

# Perform type OSVS - If yes, the exit point of any currently
# executing perform is recognized if reached.
perform-osvs:			no # TO-DO: Any potentially undefined (i.e. overlapping) PERFORMS prohibited (see p. 374)

# Compute intermediate decimal results like IBM OSVS
arithmetic-osvs:		no

# MOVE like IBM (mvc); left to right, byte by byte
move-ibm:			no

# If yes, linkage-section items remain allocated
# between invocations.
sticky-linkage:			yes

# If yes, allow non-matching level numbers
relax-level-hierarchy:		no

# Allow Hex 'F' for NUMERIC test of signed PACKED DECIMAL field
hostsign:			no

# If yes, set WITH UPDATE clause as default for ACCEPT dest-item,
# except if WITH NO UPDATE clause is used
accept-update:			no

# If yes, set WITH AUTO clause as default for ACCEPT dest-item,
# except if WITH TAB clause is used
accept-auto:			yes

# If yes, DISPLAYs and ACCEPTs are, by default, done on the CRT (i.e., using
# curses).
console-is-crt:			no

# If yes, allow redefinition of the current program's name. This prevents its
# use in a prototype-format CALL/CANCEL statement.
program-name-redefinition:	no

# If yes, NO ECHO/NO-ECHO/OFF is the same as SECURE (hiding input with
# asterisks, not spaces).
no-echo-means-secure:		no

# Dialect features
# Value: 'ok', 'warning', 'archaic', 'obsolete', 'skip', 'ignore', 'error',
#        'unconformable'

alter-statement:			obsolete
comment-paragraphs:			obsolete
call-overflow:				ok
data-records-clause:			obsolete
debugging-line:				obsolete
use-for-debugging:			obsolete
listing-statements:			skip	# may be a user-defined word
title-statement:			skip	# may be a user-defined word
entry-statement:			unconformable
goto-statement-without-name:		obsolete
label-records-clause:			obsolete
memory-size-clause:			obsolete
move-noninteger-to-alphanumeric:	error
multiple-file-tape-clause:		obsolete
next-sentence-phrase:			archaic
odo-without-to:				unconformable
padding-character-clause:		ok
section-segments:			obsolete
stop-literal-statement:			obsolete
stop-identifier-statement:		ok
synchronized-clause:			ok
top-level-occurs-clause:		unconformable
value-of-clause:			obsolete
numeric-boolean:			unconformable
hexadecimal-boolean:			unconformable
national-literals:			unconformable
hexadecimal-national-literals:		unconformable
acucobol-literals:			unconformable # TO-DO: Add config option for H"..."
word-continuation:			ok
not-exception-before-exception:		unconformable
accept-display-extensions:		ok
renames-uncommon-levels:		unconformable
constant-01:				unconformable
constant-78:				ok
program-prototypes:			unconformable
reference-out-of-declaratives:		error  # check error when referring to non-USE-statement DECLARATIVE sections
numeric-value-for-edited-item:		error
incorrect-conf-sec-order:		error

# obsolete in COBOL85 and currently not available as dialect features:
# 1: All literal with numeric or numeric edited item
# 2: RERUN clause
# 3: KEY phrase of the DISABLE and ENABLE statements
# 4: ENTER statement
# 5: REVERSED phrase of the OPEN statement

# If yes, all the reserved words must be specified in a list of reserved:
# entries; the default reserved word list will not be used.
specify-all-reserved: yes

# not-reserved:
# Value: Word to be taken out of the reserved words list
# (case independent)
# Words that are in the (proposed) standard but may conflict

# reserved:
# Value: Word to make up reserved words list (case independent)
# All reserved entries listed will replace entire default reserved words list.
#   Words ending with * will be treated as context-sensitive words. This will be
# ignored if GnuCOBOL uses that word as a reserved word.
#   Entries of the form word-1=word-2 define word-1 as an alias for default
# reserved word word-2. No spaces are allowed around the equal sign.
reserved:	ACCEPT
reserved:	ACCESS
reserved:	ADD
reserved:	ADDRESS
reserved:	ADVANCING
reserved:	AFTER
reserved:	ALL
reserved:	ALPHABET
reserved:	ALPHABETIC
reserved:	ALPHABETIC-LOWER
reserved:	ALPHABETIC-UPPER
reserved:	ALPHANUMERIC
reserved:	ALPHANUMERIC-EDITED
reserved:	ALSO
reserved:	ALTER
reserved:	ALTERNATE
reserved:	AND
reserved:	ANY
reserved:	ARE
reserved:	AREA
reserved:	AREAS
reserved:	AS
reserved:	ASCENDING
reserved:	ASSIGN
reserved:	AT
reserved:	AUTHOR
reserved:	AUTO*
reserved:	AUTO-SKIP*=AUTO
reserved:	AUTOMATIC*
reserved:	BACKGROUND*=BACKGROUND-COLOR
reserved:	BACKGROUND-COLOR*
reserved:	BEEP=BELL
reserved:	BEFORE
reserved:	BELL
reserved:	BINARY
reserved:	BLANK
reserved:	BLINK
reserved:	BLOCK
reserved:	BOTTOM
reserved:	BY
reserved:	CALL
reserved:	CANCEL
reserved:	CASE-INSENSITIVE*
reserved:	CASE-SENSITIVE*
reserved:	CD
reserved:	CENTURY-DATE
reserved:	CENTURY-DAY
reserved:	CF
reserved:	CH
reserved:	CHARACTER
reserved:	CHARACTERS
reserved:	CLASS
reserved:	CLOCK-UNITS
reserved:	CLOSE
reserved:	COBOL
reserved:	CODE
reserved:	CODE-SET
reserved:	COL
reserved:	COLLATING
reserved:	COLUMN
reserved:	COMMA
reserved:	COMMON
reserved:	COMMUNICATION
reserved:	COMP
reserved:	COMP-1
reserved:	COMP-3
reserved:	COMP-4
reserved:	COMP-5
reserved:	COMP-6
reserved:	COMPUTATIONAL
reserved:	COMPUTATIONAL-1
reserved:	COMPUTATIONAL-3
reserved:	COMPUTATIONAL-4
reserved:	COMPUTATIONAL-5
reserved:	COMPUTATIONAL-6
reserved:	COMPUTE
reserved:	CONFIGURATION
# reserved:	CONSOLE*   --> mnemonic device should not be in reserved list
reserved:	CONTAINS
reserved:	CONTENT
reserved:	CONTINUE
reserved:	CONTROL
reserved:	CONTROLS
reserved:	CONVERT
reserved:	CONVERTING
reserved:	COPY
reserved:	CORR
reserved:	CORRESPONDING
reserved:	COUNT
reserved:	COUNT-MAX
reserved:	COUNT-MIN
reserved:	CRT*
reserved:	CURRENCY
reserved:	CURSOR
reserved:	CYCLE*
reserved:	DATA
reserved:	DATA-POINTER
reserved:	DATE
reserved:	DATE-AND-TIME
reserved:	DATE-COMPILED
reserved:	DATE-WRITTEN
reserved:	DAY
reserved:	DAY-AND-TIME
reserved:	DAY-OF-WEEK
reserved:	DE
reserved:	DEBUGGING
reserved:	DECIMAL-POINT
reserved:	DECLARATIVES
reserved:	DEFAULT
reserved:	DELETE
reserved:	DELIMITED
reserved:	DELIMITER
reserved:	DEPENDING
reserved:	DESCENDING
reserved:	DESTINATION
reserved:	DETAIL
reserved:	DISABLE
reserved:	DISPLAY
reserved:	DIVIDE
reserved:	DIVISION
reserved:	DOWN
reserved:	DUPLICATES
reserved:	DYNAMIC
reserved:	ECHO
reserved:	EGI
reserved:	ELSE
reserved:	EMI
reserved:	ENABLE
reserved:	END
reserved:	END-ACCEPT
reserved:	END-ADD
reserved:	END-CALL
reserved:	END-COMPUTE
reserved:	END-COPY*
reserved:	END-DELETE
reserved:	END-DIVIDE
reserved:	END-EVALUATE
reserved:	END-IF
reserved:	END-MULTIPLY
reserved:	END-OF-PAGE
reserved:	END-PERFORM
reserved:	END-READ
reserved:	END-RECEIVE
reserved:	END-REPLACE*
reserved:	END-RETURN
reserved:	END-REWRITE
reserved:	END-SEARCH
reserved:	END-START
reserved:	END-STRING
reserved:	END-SUBTRACT
reserved:	END-UNSTRING
reserved:	END-WRITE
reserved:	ENTER
reserved:	ENVIRONMENT
reserved:	EOL*
reserved:	EOP
reserved:	EOS*
reserved:	EQUAL
reserved:	ERASE
reserved:	ERROR
reserved:	ESCAPE
reserved:	ESI
reserved:	EVALUATE
reserved:	EVERY
reserved:	EXCEPTION
reserved:	EXCLUSIVE
reserved:	EXIT
reserved:	EXTEND
reserved:	EXTERNAL
reserved:	FALSE
reserved:	FD
reserved:	FILE
reserved:	FILE-CONTROL
reserved:	FILLER
reserved:	FINAL
reserved:	FIRST
reserved:	FIXED
reserved:	FOOTING
reserved:	FOR
reserved:	FOREGROUND*=FOREGROUND-COLOR
reserved:	FOREGROUND-COLOR*
reserved:	FROM
reserved:	FULL*
reserved:	FUNCTION
reserved:	GENERATE
reserved:	GIVING
reserved:	GLOBAL
reserved:	GO
reserved:	GOBACK
reserved:	GREATER
reserved:	GROUP
reserved:	HEADING
reserved:	HIGH
reserved:	HIGH-VALUE
reserved:	HIGH-VALUES
reserved:	HIGHEST-VALUE
reserved:	HIGHLIGHT
reserved:	I-O
reserved:	I-O-CONTROL
reserved:	ID
reserved:	IDENTIFICATION
reserved:	IF
reserved:	IMP
reserved:	IMP*
reserved:	IN
reserved:	INDEX
reserved:	INDEXED
reserved:	INDICATE
reserved:	INITIAL
reserved:	INITIAL-VALUE
reserved:	INITIALIZE
reserved:	INITIATE
reserved:	INPUT
reserved:	INPUT-OUTPUT
reserved:	INSPECT
reserved:	INSTALLATION
reserved:	INTO
reserved:	INVALID
reserved:	IS
reserved:	JUST
reserved:	JUSTIFIED
reserved:	KEY
reserved:	LABEL
reserved:	LAST
reserved:	LEADING
reserved:	LEFT
reserved:	LENGTH
reserved:	LESS
reserved:	LIKE
reserved:	LIMIT
reserved:	LIMITS
reserved:	LINAGE
reserved:	LINAGE-COUNTER
reserved:	LINE
reserved:	LINE-COUNTER
reserved:	LINES
reserved:	LINKAGE
reserved:	LOCK
reserved:	LOW
reserved:	LOW-VALUE
reserved:	LOW-VALUES
reserved:	LOWEST-VALUE
reserved:	LOWLIGHT
reserved:	MANUAL*
reserved:	MAX-VALUE
reserved:	MEMORY
reserved:	MERGE
reserved:	MESSAGE
reserved:	MIN-VALUE
reserved:	MODE
reserved:	MODULES
reserved:	MOVE
reserved:	MULTIPLE*
reserved:	MULTIPLY
reserved:	NATIVE
reserved:	NEGATIVE
reserved:	NEXT
reserved:	NO
reserved:	NOT
reserved:	NULL
reserved:	NULLS
reserved:	NUMBER
reserved:	NUMERIC
reserved:	NUMERIC-EDITED
reserved:	OBJECT-COMPUTER
reserved:	OCCURS
reserved:	OF
reserved:	OFF
reserved:	OMITTED
reserved:	ON
reserved:	OPEN
reserved:	OPTIONAL
reserved:	OR
reserved:	ORDER
reserved:	ORGANIZATION
reserved:	OTHER
reserved:	OUTPUT
reserved:	OVERFLOW
reserved:	PACKED-DECIMAL
reserved:	PADDING
reserved:	PAGE
reserved:	PAGE-COUNTER
reserved:	PARAGRAPH*
reserved:	PERFORM
reserved:	PF
reserved:	PH
reserved:	PIC
reserved:	PICTURE
reserved:	PLUS
reserved:	POINTER
reserved:	POSITION
reserved:	POSITIVE
reserved:	PREVIOUS*
reserved:	PRINTING
reserved:	PROCEDURE
reserved:	PROCEDURE-NAME
reserved:	PROCEDURES
reserved:	PROCEED
reserved:	PROGRAM
reserved:	PROGRAM-ID
reserved:	PROMPT
reserved:	PURGE
reserved:	QUEUE
reserved:	QUOTE
reserved:	QUOTES
reserved:	RANDOM
reserved:	RD
reserved:	READ
reserved:	RECEIVE
reserved:	RECORD
reserved:	RECORDING
reserved:	RECORDS
reserved:	REDEFINES
reserved:	REEL
reserved:	REFERENCE
reserved:	REFERENCES
reserved:	RELATIVE
reserved:	RELEASE
reserved:	REMAINDER
reserved:	REMARKS
reserved:	REMOVAL
reserved:	RENAMES
reserved:	REPLACE
reserved:	REPLACING
reserved:	REPORT
reserved:	REPORTING
reserved:	REPORTS
reserved:	REQUIRED*
reserved:	RERUN
reserved:	RESERVE
reserved:	RESET
reserved:	RETURN
reserved:	RETURN-CODE
reserved:	RETURNING
reserved:	REVERSE
reserved:	REVERSE-VIDEO
reserved:	REVERSED
reserved:	REWIND
reserved:	REWRITE
reserved:	RF
reserved:	RH
reserved:	RIGHT
reserved:	ROUNDED
reserved:	RUN
reserved:	SAME
reserved:	SCREEN
reserved:	SD
reserved:	SEARCH
reserved:	SECTION
reserved:	SECURE
reserved:	SECURITY
reserved:	SEGMENT
reserved:	SEGMENT-LIMIT
reserved:	SELECT
reserved:	SEND
reserved:	SENTENCE
reserved:	SEPARATE
reserved:	SEQUENCE
reserved:	SEQUENTIAL
reserved:	SET
reserved:	SIGN
reserved:	SIZE
reserved:	SORT
reserved:	SORT-MERGE
reserved:	SOURCE
reserved:	SOURCE-COMPUTER
reserved:	SPACE
reserved:	SPACES
reserved:	SPECIAL-NAMES
reserved:	STANDARD
reserved:	STANDARD-1
reserved:	STANDARD-2
reserved:	START
reserved:	STATUS
reserved:	STOP
reserved:	STRING
reserved:	SUB-QUEUE-1
reserved:	SUB-QUEUE-2
reserved:	SUB-QUEUE-3
reserved:	SUBTRACT
reserved:	SUM
reserved:	SUPPRESS
reserved:	SYMBOLIC
reserved:	SYNC
reserved:	SYNCHRONIZED
reserved:	TAB
reserved:	TABLE
reserved:	TALLYING
reserved:	TAPE
reserved:	TERMINAL
reserved:	TERMINATE
reserved:	TEST
reserved:	TEXT
reserved:	THAN
reserved:	THEN
reserved:	THROUGH
reserved:	THRU
reserved:	TIME
reserved:	TIMES
reserved:	TO
reserved:	TOP
reserved:	TRAILING
reserved:	TRIMMED*
reserved:	TRUE
reserved:	TYPE
reserved:	UNDERLINE*
reserved:	UNIT
reserved:	UNLOCK
reserved:	UNSTRING
reserved:	UNTIL
reserved:	UP
reserved:	UPDATE
reserved:	UPON
reserved:	USAGE
reserved:	USE
reserved:	USING
reserved:	VALUE
reserved:	VALUES
reserved:	VARIABLE
reserved:	VARYING
reserved:	WHEN
reserved:	WHEN-COMPILED
reserved:	WHILE*
reserved:	WITH
reserved:	WORDS
reserved:	WORKING-STORAGE
reserved:	WRITE
reserved:	YYYYDDD*
reserved:	YYYYMMDD*
reserved:	ZERO
reserved:	ZEROES
reserved:	ZEROS
